'use strict';

const _     = require('lodash');
const async = require('neo-async');

/**
 * Built in validators.
 * @const
 */
const BUILT_IN_VALIDATOR = {
  // @formatter:off
  'required': (v, o, done) => done((v !== undefined) === o),
  '==='     : (v, o, done) => done(v === undefined ? true : _.isEqual(v, o)),
  '!=='     : (v, o, done) => done(v === undefined ? true : !_.isEqual(v, o)),
  '!!'      : (v, o, done) => done(v === undefined ? true : !!v === o),
  '<'       : (v, o, done) => done(v === undefined ? true : _.lt(v, o)),
  '<='      : (v, o, done) => done(v === undefined ? true : _.lte(v, o)),
  '>'       : (v, o, done) => done(v === undefined ? true : _.gt(v, o)),
  '>='      : (v, o, done) => done(v === undefined ? true : _.gte(v, o)),
  'is'      : (v, o, done) => done(v === undefined ? true : typeOfStrict(v, o)),
  'in'      : (v, o, done) => done(v === undefined ? true : _.includes(o, v)),
  'inrange' : (v, o, done) => done(v === undefined ? true : _.inRange(v, o[0], o[1])),
  'match'   : (v, o, done) => done(v === undefined ? true : o.test(v.toString()))
  // @formatter:on
};

/**
 * Check type of a value strictly.
 * @param {*} value
 * @param {String} type
 * @returns {Boolean}
 * @private
 */
function typeOfStrict(value, type) {
  // @formatter:off
  return  (type === 'array'    && _.isArray(value)) ||
          (type === 'object'   && _.isPlainObject(value)) ||
          (type === 'number'   && _.isNumber(value)) ||
          (type === 'function' && _.isFunction(value)) ||
          (type === 'string'   && _.isString(value));
  // @formatter:on
}


/**
 * Process a value by some given processors.
 * @param {*} value
 * @param {Array<Function>} processors
 * @returns {*}
 * @private
 */
function processValue(value, processors) {
  return processors.reduce((v, p) => p(v), value);
}

/**
 * Validate a value by a custom/built-in validator.
 * @param {*} value
 * @param {Function|String} validator
 * @param {*} opts
 * @param {Function} done
 * @private
 */
function valdiateValue(value, validator, opts, done) {
  // @formatter:off
  return _.isFunction(validator)
            ? validator(value, opts, done)
              : BUILT_IN_VALIDATOR[validator](value, opts, done);
  // @formatter:on
}

/**
 * Run a rule on an object.
 * @param {Object} rule
 * @param {Object} obj
 * @param {Object} opts
 * @param {Boolean} opts.series
 * @param {Function} done
 * @private
 */
function runRule(rule, obj, opts, done) {
  let value = _.get(obj, rule.path);
  if (rule.validator !== 'required' && value === undefined) {
    return done(null);
  }
  value = processValue(value, rule.processors);
  valdiateValue(value, rule.validator, rule.validatorOpts, (valid) => {
    if (opts.series) {
      done(valid ? null : _.pick(rule, ['path', 'message']));
    } else {
      done(null, valid ? undefined : _.pick(rule, ['path', 'message']));
    }
  });
}

/**
 * Validate an object by given rules.
 * @param {Object} obj
 * @param {Array} rules
 * @param {Object} opts
 * @param {Boolean} opts.series
 * @param {Function} done
 * @private
 */
function validateObject(obj, rules, opts, done) {
  const eachRule = (rule, next) => runRule(rule, obj, opts, next);
  if (opts.series) {
    async.mapSeries(rules, eachRule, done);
  } else {
    async.map(rules, eachRule, (_null, errors) => {
      errors = _.compact(errors);
      done(_.isEmpty(errors) ? null : errors);
    });
  }
}

/**
 * Validate a validator.
 * @param {Function|String} validator
 * @throws {TypeError} If the validator is invalid.
 * @private
 */
function validateValidator(validator) {
  if (!_.isFunction(validator) && !_.isFunction(BUILT_IN_VALIDATOR[validator])) {
    throw new TypeError(`Expect a standard validator but got "${validator}"`);
  }
}

/**
 * Process a schema.
 * @param {Array.<Array>} schema
 * @returns {Array}
 * @throws {TypeError} If the scheme is invalid.
 * @private
 */
function processSchema(schema) {
  if (!_.isArray(schema)) {
    throw new TypeError(`Expect a standard schema but got "${schema}"`);
  }
  return _.map(schema, (ruleSchema) => {
    if (!_.isArray(ruleSchema) || ruleSchema.length < 4 || ruleSchema.length > 5) {
      throw new TypeError(`Expect a standard rule schema but got "${ruleSchema}"`);
    }

    const hasProcessors = ruleSchema.length > 4;
    const path          = ruleSchema[0];
    const processors    = hasProcessors ? _.castArray(ruleSchema[1]) : [];
    const validator     = hasProcessors ? ruleSchema[2] : ruleSchema[1];
    validateValidator(validator);
    const validatorOpts = hasProcessors ? ruleSchema[3] : ruleSchema[2];
    const message       = hasProcessors ? ruleSchema[4] : ruleSchema[3];
    return {
      path,
      processors,
      validator,
      validatorOpts,
      message
    };
  });
}

/**
 * Create a validator.
 * @param {Array.<Array>} scheme
 * @param {Object} [opts]
 * @param {Boolean} [opts.series]
 */
function create(scheme, opts) {
  opts = _.defaults({}, opts, {
    series: false
  });

  const rules = processSchema(scheme);
  return (obj, done) => validateObject(obj, rules, opts, done);
}

module.exports = {
  create
};

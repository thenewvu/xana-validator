'use strict';

const path    = require('path');
const chai    = require('chai');
const fs      = require('fs');
const yaml    = require('js-yaml');
const decache = require('decache');
const expect  = chai.expect;

const testDir       = path.dirname(__filename);
const testDataFiles = fs.readdirSync(testDir)
												.filter((file) => file.endsWith('.yaml'))
												.map((file) => path.join(testDir, file));

testDataFiles.forEach((file) => {
	const testCases = yaml.load(fs.readFileSync(file));
	Object.keys(testCases).forEach((describeName) => {
		describe(describeName, function () {
			Object.keys(testCases[describeName]).forEach((itName) => {
				it(itName, function (done) {
					const itData = testCases[describeName][itName];
					decache('../src/index.js');
					const validator = require('../src/index.js');
					validator.create(itData.schema, itData.opts)(itData.obj, (err) => {
						expect(err).to.deep.equal(itData.err);
						done();
					});
				});
			});
		});
	});
});
